/**
 * Created by Cornell on 2/20/2015.
 */

function loadJSON(p_fname) {
  $.getJSON(p_fname, function(p_json) {
    $("#tabs").zinoTabs("destroy");

    $("#heading").html(p_json.metadata.title);
    $("#tabs").html("<ul id='tabList'></ul>");

    var l_tabIndex = 0;

    p_json.tabs.forEach(function(p_tab) {
      p_tab.forEach(function(p_element) {
        if (p_element.hasOwnProperty("title")) {
          $("#tabList").append("<li><a href='#tabs-" + l_tabIndex + "'>" + p_element.title + "</a></li>");
          $("#tabs").append("<div id='tabs-" + l_tabIndex + "'>");
        }
        else if (p_element.hasOwnProperty("tag")) {
          var l_element = createElement(p_element.tag, p_element.body);

          if (p_element.hasOwnProperty("tooltip"))
          {
            l_element.addClass("tooltip");
            l_element.attr("title", p_element.tooltip);
          }

          if (p_element.hasOwnProperty("href"))
          {
            l_element.attr("href", p_element.href);
          }

          if (p_element.hasOwnProperty("class")) {
            l_element.addClass(p_element.class);
          }

          if (p_element.hasOwnProperty("style"))
          {
            l_element.attr("style", p_element.style);
          }

          $("#tabs-" + l_tabIndex).append(l_element);
        }
        else if (p_element.hasOwnProperty("diagram")) {
          var l_diagramType = p_element.diagram;

          if (l_diagramType === "venn") {
            constructVennDiagram(p_element, l_tabIndex);
          }
        }
      });

      $("#tabs").append("</div>");

      l_tabIndex++;
    });

    setupTabs();
    setupTooltips();

    /*var tab = (p_fname.substring(p_fname.lastIndexOf("/") + 1, p_fname.lastIndexOf(".")));
    var url = "";
    if (document.location.hostname == "localhost") {
      url = ('http://localhost/discrete/index.html?tab=' + tab);
    }
    else {
      url = ('http://bojjenclon.com/projects/school/discrete/index.html?tab=' + tab);
    }

    window.history.pushState(tab, tab, url);*/
  });
}

function createElement(p_tag, p_body) {
  var l_element;

  if (p_tag === "br") {
    l_element = $("<br />");
  }
  else if (p_tag === "hr") {
    l_element = $("<hr />");
  }
  else {
    l_element = $("<" + p_tag + ">" + p_body + "</" + p_tag + ">");
  }

  return l_element;
}

function setupTabs() {
  $("#tabs").zinoTabs();
}

function setupTooltips() {
  $(".tooltip").zinoTooltip({
    follow: true,
    //opacity: 0.8,
    offset: [5, 10],
    showAfter: 300,
    hideAfter: 300
  });
}

function constructVennDiagram(p_element, p_tabIndex) {
  var l_element = $("<div id='" + p_element.id + "' class='diagram'></div>");
  $("#tabs-" + p_tabIndex).append(l_element);

  var l_chart = venn.VennDiagram();
  d3.select("#" + p_element.id).datum(p_element.data).call(l_chart);
}
